Scenario: Login
Given on Login page
When Touching the textbox
Then the keyboard has to fade up from the button side

When Touching text on the keyboard
Then Text has a show on the textbox selected

When entering incorrect email form
And Touch Login button
Then Textbox has changed to the red box with “กรุณากรอกอีเมล” in red text

When entering correct email form
And entering incorrect password
And Touch the Login button
Then Textbox has changed to the red box with “กรุณากรอกรหัสผ่าน” in red text

---------------------------------------------

Scenario: Login success
Given on login page
When entering the email and password
Then change to PIN page

---------------------------------------------

Scenario: Login failed because email is wrong
Given on login page
When entering the wrong email and correct password
Then notify pop-up Warning Invalid user or password
And touching the OK button
Then pop-up Warning has close

---------------------------------------------

Scenario: Login failed because the password is wrong
Given on login page
When entering the correct email and wrong password
Then notify pop-up Warning Invalid user or password
And touching the OK button
Then pop-up Warning has close
Can not
