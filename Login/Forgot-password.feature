Scenario: Correct email entered
Given on login page
When touching the "ลืมรหัสผ่าน" button
Then login page has changed to "ตั้งค่ารหัสผ่านใหม่" page

When entering the correct email
Then pop-up has shown up with "ส่งรหัสผ่านใหม่ไปยังอีเมลแล้ว"
And touching the "ตกลง" button
Then pop-up has close

---------------------------------------------

Scenario: The wrong email entered
Given on login page
When touching the "ลืมรหัสผ่าน" button
Then login page has changed to "ตั้งค่ารหัสผ่านใหม่" page

When entering the incorrect email
Then pop-up has shown up with "ไม่พบบัญชีของคุณ"
And touching the "ปิด" button
Then pop-up has close
