Scenario: Done - Edit General
Given on "สั่งและชำระเงิน" page
When touching the "ตั้งค่า" Tab
Then "สั่งและชำระเงิน" page has changed to "ตั้งค่า" Tab and "การชำระเงิน" page
 
When touching the Service Charge toggle
Then toggle has changed from grey color to orange color with textbox "อัตราค่าบริการ"

When touching the textbox
Then can edit a number with a limit of 100%
And entering number

When touching the VAT toggle
Then toggle has changed from grey color to orange color with textbox "อัตราค่าบริการ"

When touching the textbox
Then can edit a number with a limit of 100%
And entering number

When touching the "วิธีการปัดเศษ" drop-down box
Then show up "ไม่มี", "ปัดเศษไปจำนวนใกล้เคียง", "ปัดเศษขึ้น", "ปัดเศษลง"
And Select "ไม่มี"/ "ปัดเศษไปจำนวนใกล้เคียง"/ "ปัดเศษขึ้น"/ "ปัดเศษลง"
Then drop-down box has shown up "ไม่มี"/ "ปัดเศษไปจำนวนใกล้เคียง"/ "ปัดเศษขึ้น"/ "ปัดเศษลง"

When touching the "บันทึก" button
Then toast Save success

---------------------------------------------

Scenario: Cancel - Edit General
Given on "สั่งและชำระเงิน" page
When touching the "ตั้งค่า" Tab
Then "สั่งและชำระเงิน" page has changed to "ตั้งค่า" Tab and "การชำระเงิน" page
 
When touching the Service Charge toggle
Then toggle has changed from grey color to orange color with textbox "อัตราค่าบริการ"

When touching the textbox
Then can edit a number with a limit of 100%
And entering number

When touching the VAT toggle
Then toggle has changed from grey color to orange color with textbox "อัตราค่าบริการ"

When touching the textbox
Then can edit a number with a limit of 100%
And entering number

When touching the "วิธีการปัดเศษ" drop-down box
Then show up "ไม่มี", "ปัดเศษไปจำนวนใกล้เคียง", "ปัดเศษขึ้น", "ปัดเศษลง"
And Select "ไม่มี"/ "ปัดเศษไปจำนวนใกล้เคียง"/ "ปัดเศษขึ้น"/ "ปัดเศษลง"
Then drop-down box has shown up "ไม่มี"/ "ปัดเศษไปจำนวนใกล้เคียง"/ "ปัดเศษขึ้น"/ "ปัดเศษลง"

When touching the another Tab
And touching the "ตั้งค่า" Tab
Then Service Charge, VAT, "การปัดเศษ" has not change as amended
