Scenario: Drawer Setting
Given on "รอบการขาย" page
When touching the "เปิด/ปิด การใช้งานถาดเก็บเงิน" toggle
Then The "เปิด/ปิด การใช้งานถาดเก็บเงิน" toggle has changed from grey color to orange

When touching the "จำนวนเงินเริ่มต้น" textbox
Then can edit
And entering number

When touching the "ส่งรายงานอัตโนมัติ" toggle
Then "ส่งรายงานอัตโนมัติ" toggle has changing from grey color to orange color

When touching the "เพิ่มอีเมล" button
Then textbox has shown up
And touching the textbox
Then can edit
And entering email with correct email from

When touching the "บัันทึก" button
Then toast Save success
