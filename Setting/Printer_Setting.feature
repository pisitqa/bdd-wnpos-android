Scenario: Search Printer from "สั่งและชำระเงิน" Tab
Given on "สั่งและชำระเงิน" Tab
When touching the "ตั้งค่า" Tab 
And touching the "เครื่องพิมพ์" Tab
Then "เชื่อมต่อเครื่องพิมพ์" page has shown up with auto searching printer

---------------------------------------------

Scenario: Search Printer on "เชื่อมต่อเครื่องพิมพ์" page
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the "รีเฟรช" button
Then the "รีเฟรช" button changed to "กำลังค้นหาเครื่องพิมพ์" and printer un-connected has shown up

---------------------------------------------

Scenario: Connected printer
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the "เชื่อมต่อ" button
Then pop-up "ตั้งค่าเครื่องพิมพ์" has shown up

When touching the "ชื่อเครื่องพิมพ์" textbox
Then keyboard has shown up from the bottom side and can edit
And touching the keyboard to entering text or number
Then text or number has shown up on textbox

When touching the "พิมพ์ใบแจ้งหนี้และใบเสร็จ" toggle
Then toggle grey color has changed to orange color and show "พิมพ์ใบเสร็จอัตโมัติ" checkbox & "พิมพ์ใบเสร็จ ซ้ำ 2 ใบ" checkbox under the toggle
And touching the "พิมพ์ใบเสร็จอัตโมัติ" checkbox
Then checkbox has changed to orange color with white arrow

When touching the "พิมพ์ใบรายการในครัว" toggle
Then toggle grey color has changed to orange color and show "ประเภทกระดาษ", "วิธีตัดใบรายการ", "ขนาดตัวอักษร" and "อื่นๆ" heading radio button under the toggle
And touching "บันทึก" button
Then "บันทึก" button has changed to "กำลังบันทึก" text and pop-up "ตั้งค่าเครื่องพิมพ์" has closed

---------------------------------------------

Scenario: Add print order
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the "คั้งค่าเครื่องพิมพ์ครัว" button
Then pop-up "ตั้งค่าเครื่องพิมพ์ครัว" has shown up

When touching the "เลือกทั้งหมด" checkbox
Then All checkbox has changed to white color
And touching the "บันทึก" button
Then "บันทึก" button has changed to "กำลังบันทึก" and pop-up "ตั้งค่าเครื่องพิมพ์ครัว" has closed 

---------------------------------------------

Scenario: Edit print order
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the "คั้งค่าเครื่องพิมพ์ครัว" button
Then pop-up "ตั้งค่าเครื่องพิมพ์ครัว" has shown up

When touching some checkbox
Then checkbox some checkbox has changed to white color
And touching the "บันทึก" button
Then "บันทึก" button has changed to "กำลังบันทึก" and pop-up "ตั้งค่าเครื่องพิมพ์ครัว" has closed 

---------------------------------------------

Scenario: Reset print order
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the "คั้งค่าเครื่องพิมพ์ครัว" button
Then pop-up "ตั้งค่าเครื่องพิมพ์ครัว" has shown up

When touching the "เลือกทั้งหมด" checkbox
Then All checkbox has changed to white color
And touching the "รีเซตตัวเลือก" button
Then All checkbox has changed to orange color with white arrow

---------------------------------------------

Scenario: Edit printer
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the printer on the row
Then pop-up "ตั้งค่าเครื่องพิมพ์" has shown up

When touching the "ชื่อเครื่องพิมพ์" textbox
Then keyboard has shown up from the bottom side and can edit
And touching the keyboard to entering text or number
Then text or number has shown up on textbox

When touching the "พิมพ์ใบแจ้งหนี้และใบเสร็จ" toggle
Then toggle orange color has changed to grey color and "พิมพ์ใบเสร็จอัตโมัติ" checkbox & "พิมพ์ใบเสร็จ ซ้ำ 2 ใบ" checkbox under the toggle has put in

When touching the "พิมพ์ใบรายการในครัว" toggle
Then toggle orange color has changed to grey color and "ประเภทกระดาษ", "วิธีตัดใบรายการ", "ขนาดตัวอักษร" and "อื่นๆ" heading radio button under the toggle has put in
And touching "บันทึก" button
Then "บันทึก" button has changed to "กำลังบันทึก" text and pop-up "ตั้งค่าเครื่องพิมพ์" has closed

---------------------------------------------

Scenario: Edit printer
Given on "เชื่อมต่อเครื่องพิมพ์" page
When touching the printer on the row
Then pop-up "ตั้งค่าเครื่องพิมพ์" has shown up
And touching the "ลบเครื่องพิมพ์" button
Then "ลบเครื่องพิมพ์" button has changed to "กำลังลบ" and pop-up has closed

