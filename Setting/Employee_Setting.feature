Scenario: Create Employee
Given On "พนักงาน" page
When touching the "เพิ่มพนักงาน" button
Then pop-up "เพิ่มพนักงาน" has shown up
And touching the "ืชื่อบัญชีพนักงาน" textbox
Then can edit
And entering text or number
Then text has shown up on textbox

And touching the "อีเมล" textbox
Then can edit
And entering text or number with correct email form
Then text has shown up on textbox

And touching the "ตำแหน่งพนักงาน" drop-down box
Then "บริกร", "แคชเชียร์", "ผู้ช่วยผู้จัดการ" has shown up
And select "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ"
Then "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ" has show on drop-down box

When touch the "บันทึก" button
Then Employee has created

---------------------------------------------

Scenario: Cancel Employee
Given On "พนักงาน" page
When touching the "เพิ่มพนักงาน" button
Then pop-up "เพิ่มพนักงาน" has shown up
And touching the "ืชื่อบัญชีพนักงาน" textbox
Then can edit
And entering text or number
Then text has shown up on textbox

And touching the "อีเมล" textbox
Then can edit
And entering text or number with correct email form
Then text has shown up on textbox

And touching the "ตำแหน่งพนักงาน" drop-down box
Then "บริกร", "แคชเชียร์", "ผู้ช่วยผู้จัดการ" has shown up
And select "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ"
Then "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ" has show on drop-down box

When touch the "ยกเลิก" button
Then Employee has not created

---------------------------------------------

Scenario: Edit Employee
Given On "พนักงาน" page
When touching the "แก้ไข" button
Then pop-up "แก้ไขพนักงาน" has shown up
And touching the "ืชื่อบัญชีพนักงาน" textbox
Then can edit
And entering text or number
Then text has shown up on textbox

And touching the "อีเมล" textbox
Then can not edit
And entering text or number with correct email form
Then text has shown up on textbox

And touching the "ตำแหน่งพนักงาน" drop-down box
Then "บริกร", "แคชเชียร์", "ผู้ช่วยผู้จัดการ" has shown up
And select "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ"
Then "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ" has show on drop-down box

When touching the "บันทึก" button
Then Employee has changed

---------------------------------------------

Scenario: Cancel Edit Employee
Given On "พนักงาน" page
When touching the "แก้ไข" button
Then pop-up "แก้ไขพนักงาน" has shown up
And touching the "ืชื่อบัญชีพนักงาน" textbox
Then can edit
And entering text or number
Then text has shown up on textbox

And touching the "อีเมล" textbox
Then can not edit
And entering text or number with correct email form
Then text has shown up on textbox

And touching the "ตำแหน่งพนักงาน" drop-down box
Then "บริกร", "แคชเชียร์", "ผู้ช่วยผู้จัดการ" has shown up
And select "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ"
Then "บริกร"/ "แคชเชียร์"/ "ผู้ช่วยผู้จัดการ" has show on drop-down box

When touching the "ยกเลิก" button
Then Employee has not changed

---------------------------------------------

Scenario: Delete Employee
Given On "พนักงาน" page
When touching the "แก้ไข" button
Then pop-up "แก้ไขพนักงาน" has shown up
And touching the "ลบบัญชี" button
Then pop-up "ลบบัญชีพนักงาน" has shown up
And touching the "ลบบัญชี" button
Then Employee has deleted

---------------------------------------------

Scenario: Cancel Delete Employee
Given On "พนักงาน" page
When touching the "แก้ไข" button
Then pop-up "แก้ไขพนักงาน" has shown up
And touching the "ลบบัญชี" button
Then pop-up "ลบบัญชีพนักงาน" has shown up
And touching the "ยกเลิก" button
Then pop-up "แก้ไขพนักงาน" has closed

