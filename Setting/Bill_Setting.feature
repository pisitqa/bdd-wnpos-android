Scenario: Un-entering
Given on "การชำระเงิน" Tab
When touching "รูปแบบใบเสร็จ" Tab
Then "การชำระเงิน" has changed to "รูปแบบใบเสร็จ" page

When touching the "บันทึก" button without entering
Then "บันทึก" button has changed to "กำลังบันทึก" button with grey color and toast Save success 

---------------------------------------------

Scenario: entering Success
Given on "การชำระเงิน" Tab
When touching "รูปแบบใบเสร็จ" Tab
Then "การชำระเงิน" has changed to "รูปแบบใบเสร็จ" page

When touching "หมายเลขประจำเครื่อง POS" textbox
Then keyboard has show up from the bottom side and can edit with limit 40 character
And entering some text or number
Then text or number has show on the textbox

When touching "หมายเลขผู้เสียภาษี" textbox
Then keyboard has show up from the bottom side and can edit with limit 13 character without text
And entering number
Then number has show on the textbox

When touching "ชื่อใบแจ้งหนี้" textbox
Then keyboard has show up from the bottom side and can edit with limit 10 character
And entering some text or number
Then text or number has show on the textbox

When touching "หมายเลขสุดท้ายของใบแจ้งหนี้" textbox
Then keyboard has show up from the bottom side and can edit with limit 8 character without text
And entering some number
Then number has show on the textbox

When touching the "บันทึก" button
Then "บันทึก" button has changed to "กำลังบันทึก" button with grey color and toast Save success 

Scenario: entering POS name with POS number
Given on "การชำระเงิน" Tab
When touching "รูปแบบใบเสร็จ" Tab
Then "การชำระเงิน" has changed to "รูปแบบใบเสร็จ" page

When touching "ชื่อใบแจ้งหนี้" textbox
Then keyboard has show up from the bottom side and can edit with limit 10 character
And entering some text or number
Then text or number has show on the textbox

When touching the "บันทึก" button
Then "บันทึก" button has changed to "กำลังบันทึก" button with "หมายเลขสุดท้ายของใบแจ้งหนี้" textbox red color and show "ต้องกรอกหมายเลขสุดท้ายของใบแจ้งหนี้" with red color under textbox