Scenario: Correct PIN to clock in
Given on PIN page
When touching "เวลาเข้า/ออก งาน" button
Then PIN page has changed to "บันทึกเวลาเข้า/ออก" page
And entering the correct PIN
Then pop-up  "บันทึกเวลาเข้างาน" has shown up
And Touch the "แตะเพื่อปิดหน้าต่าง"

---------------------------------------------

Scenario: Wrong PIN to clock in
Given on PIN page
When touching "เวลาเข้า/ออก งาน" button
Then PIN page has changed to "บันทึกเวลาเข้า/ออก" page
And entering the wrong PIN
Then text "รหัสเข้างานไม่ถูกต้อง" has show up with red

---------------------------------------------

Scenario: Correct PIN to clock out
Given on PIN page
When touching "เวลาเข้า/ออก งาน" button
Then PIN page has changed to "บันทึกเวลาเข้า/ออก" page
And entering the correct PIN
Then pop-up  "บันทึกเวลาออกงาน" has shown up
And Touch the "แตะเพื่อปิดหน้าต่าง"

---------------------------------------------

Scenario: Wrong PIN to clock out
Given on PIN page
When touching "เวลาเข้า/ออก งาน" button
Then PIN page has changed to "บันทึกเวลาเข้า/ออก" page
And entering the wrong PIN
Then text "รหัสเข้างานไม่ถูกต้อง" has show up with red